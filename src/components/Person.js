import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardTitle,
  Col,
  Row,
} from "reactstrap";
import { connect } from "react-redux";
import * as API from "../actions/person";

class Person extends Component {
  handleRemove = () => {
    // panggil dispatch remove
    this.props.removePerson({ id: this.props.id });
  };
  render() {
    const { name, address, phoneNumber, photo } = this.props;
    return (
      <Card className="container-fluid p-4 text-center">
        <CardImg top src={photo} />
        <CardBody>
          <CardTitle>{name}</CardTitle>
          <Row className="my-2">
            <Col>
              {address}, {phoneNumber}
            </Col>
          </Row>
          <Button onClick={this.handleRemove} color="danger">
            Remove
          </Button>
        </CardBody>
      </Card>
    );
  }
}

const mapStateToProps = () => ({
  // data: state.persons,
});

const mapDispatchToProps = {
  removePerson: API.removePerson,
};

export default connect(mapStateToProps, mapDispatchToProps)(Person);

// const Person = ({ name, address, phoneNumber, photo }) => (
//   <Card className="container-fluid p-4 text-center">
//     <CardImg top src={photo} />
//     <CardBody>
//       <CardTitle>{name}</CardTitle>
//       <Row className="my-2">
//         <Col>
//           {address}, {phoneNumber}
//         </Col>
//       </Row>
//       <Button color="danger">Remove</Button>
//     </CardBody>
//   </Card>
// );

// export default Person;
