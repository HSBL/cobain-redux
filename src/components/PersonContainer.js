import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Container, Row } from "reactstrap";
import Person from "./Person";
import * as API from "../actions/person";

class PersonContainer extends Component {
  componentDidMount() {
    this.props.getAllPerson();
  }
  createCard = (personProps) => (
    <Col key={personProps.id}>
      <Person {...personProps} />
    </Col>
  );
  createRow = (rows) => (
    <Row key={`${Math.random()}-${Date.now()}`}>
      {rows.map((i) => this.createCard(Object.assign(i, { key: i.id })))}
    </Row>
  );
  render() {
    const { data } = this.props;
    const contents = [];
    for (let i = 0; i < data.length; i += 3) {
      contents.push(data.slice(i, i + 3));
    }
    return <Container>{contents.map((i) => this.createRow(i))}</Container>;
  }
}

const mapStateToProps = (state) => ({
  data: state.persons,
});

const mapDispatchToProps = {
  getAllPerson: API.getAllPerson,
  addPerson: API.addPerson,
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonContainer);
