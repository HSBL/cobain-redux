import { ADD, INIT, REMOVE } from "../actions/person";

const initialState = [];

export default function persons(state = initialState, action) {
  switch (action.type) {
    case ADD:
      return [...state, action.payload];
    case INIT:
      return action.payload;
    case REMOVE:
      return [...state.filter((i) => i.id !== action.payload.id)];
    default:
      return state;
  }
}
