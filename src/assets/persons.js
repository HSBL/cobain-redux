export default {
  // pura - pura hit API
  all: () =>
    Promise.resolve([
      {
        id: 1,
        name: "Ryan",
        address: "Los Feliz",
        phoneNumber: "+1-123-123",
        photo:
          "https://images.unsplash.com/photo-1553272725-086100aecf5e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxlZGl0b3JpYWwtZmVlZHwxfHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=600&q=60",
      },
      {
        id: 2,
        name: "Ronaldo",
        address: "Los Feliz",
        phoneNumber: "+1-123-123",
        photo:
          "https://images.unsplash.com/photo-1553272725-086100aecf5e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxlZGl0b3JpYWwtZmVlZHwxfHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=600&q=60",
      },
      {
        id: 3,
        name: "Messi",
        address: "Los Feliz",
        phoneNumber: "+1-123-123",
        photo:
          "https://images.unsplash.com/photo-1553272725-086100aecf5e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxlZGl0b3JpYWwtZmVlZHwxfHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=600&q=60",
      },
      {
        id: 4,
        name: "Neymar",
        address: "Feliz",
        phoneNumber: "+1-123-123",
        photo:
          "https://images.unsplash.com/photo-1553272725-086100aecf5e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxlZGl0b3JpYWwtZmVlZHwxfHx8ZW58MHx8fHw%3D&auto=format&fit=crop&w=600&q=60",
      },
    ]),
};
